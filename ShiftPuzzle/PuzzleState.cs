﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiftPuzzle
{
    public struct PuzzleState
    {
        const int EMPTY = 0;
        private readonly int emptyIndex;
        public (int width, int height) Dimensions { get; }

        public PuzzleState(int[] data, int xCount, int yCount, int emptyCellIndex)
        {
            this.Dimensions = (xCount, yCount);
            this.State = data;
            if(data[emptyCellIndex] != EMPTY)
            {
                throw new ArgumentException("Given Empty Cell Index is incorrect.", nameof(emptyCellIndex));
            }
            this.emptyIndex = emptyCellIndex;
            this.MoveActions = PuzzleStateHelper.GetAllowedMovementActions(this.emptyIndex, this.Dimensions);
            StringBuilder identityBuilder = new StringBuilder((int)(xCount * yCount));
            foreach(var i in State)
            {
                identityBuilder.Append(i).Append(".");
            }
            identityBuilder.Length--;
            this.Identity = identityBuilder.ToString();
        }

        public PuzzleState(int xCount, int yCount, int shuffles = 0)
        {
            this.Dimensions = (xCount, yCount);
            var array = new int[xCount * yCount];
            for(int i = 0; i < array.Length - 1; i++)
            {
                array[i] = i + 1;
            }
            array[^1] = EMPTY;
            //Shuffle, if set
            this.emptyIndex = array.Length - 1;
            Random rand = new Random();
            while(shuffles-- > 0)
            {
                
                this.MoveActions = PuzzleStateHelper.GetAllowedMovementActions(this.emptyIndex, this.Dimensions);

                // Get a random action and apply
                this.emptyIndex = MakeMoveInPlace(this.MoveActions[rand.Next(this.MoveActions.Count)], this.Dimensions, this.emptyIndex, ref array);

            }
            this.State = array;
            this.MoveActions = PuzzleStateHelper.GetAllowedMovementActions(this.emptyIndex, this.Dimensions);
            StringBuilder identityBuilder = new StringBuilder((int)(xCount * yCount));
            foreach (var i in State)
            {
                identityBuilder.Append(i).Append(".");
            }
            identityBuilder.Length--;
            this.Identity = identityBuilder.ToString();

        }

        public PuzzleState(int xCount, int yCount, string state)
        {
            var split = state.Trim().Split('.');
            if(split.Length != xCount * yCount)
            {
                throw new ArgumentException("Given input state is invalid as it does not have the required quantity of elements.");
            }
            List<int> indices = new List<int>();
            foreach(var input in split)
            {
                var canParse = int.TryParse(input, out var output);
                if (canParse)
                {
                    indices.Add(output);
                }
                else
                {
                    throw new ArgumentException("Input Contains Data that is not an int.");
                }
            }
            var distinctCount = indices.Distinct().Count();
            var boundsFilterCount = indices.Where(e => e >= 0 && e <= xCount * yCount - 1).Count();
            if(distinctCount != boundsFilterCount || distinctCount != indices.Count)
            {
                throw new ArgumentException("Input Contains data that has either indices out of range or duplicate indices");
            }
            this.Dimensions = (xCount, yCount);
            this.State = indices.ToArray();
            this.emptyIndex = indices.IndexOf(0);
            this.MoveActions = PuzzleStateHelper.GetAllowedMovementActions(this.emptyIndex, this.Dimensions);
            StringBuilder identityBuilder = new StringBuilder((int)(xCount * yCount));
            foreach (var i in State)
            {
                identityBuilder.Append(i).Append(".");
            }
            identityBuilder.Length--;
            this.Identity = identityBuilder.ToString();
        }

        public string Identity { get; }

        public int[] State { get; }

        public IList<Actions.MoveAction> MoveActions { get; }

        public PuzzleState MakeMove(Actions.MoveAction move)
        {
            if (!this.MoveActions.Contains(move))
            {
                throw new InvalidOperationException("Given Move operation cannot be applied to the current state.");
            }

            var newArray = new int[this.State.Length];
            Array.Copy(this.State, newArray, this.State.Length);
            var newSwapIndex = MakeMoveInPlace(move, this.Dimensions, this.emptyIndex, ref newArray);

            return new PuzzleState(newArray, this.Dimensions.width, this.Dimensions.height, newSwapIndex);
        }


        private static int MakeMoveInPlace(Actions.MoveAction move, (int width, int height) dimensions, int emptyIndex, ref int[] array)
        {
            var swapIndex = move switch
            {
                Actions.MoveAction.Up => emptyIndex - dimensions.width,
                Actions.MoveAction.Down => emptyIndex + dimensions.width,
                Actions.MoveAction.Left => emptyIndex + 1,
                Actions.MoveAction.Right => emptyIndex - 1,
                _ => throw new InvalidOperationException("Unsupported Action."),
            };
            array[emptyIndex] = array[swapIndex];
            array[swapIndex] = EMPTY;

            return swapIndex;
        }

        

    }
}
