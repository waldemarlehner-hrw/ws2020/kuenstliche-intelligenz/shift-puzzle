﻿using ShiftPuzzle.Evaluators;
using System;
using System.Collections.Generic;

namespace ShiftPuzzle
{
    public class Node : IComparable
    {

        public Node(PuzzleState state, Actions.MoveAction takenAction, IEvaluator evaluator)
        {
            this.State = state;
            this.Cost = evaluator.EvaluateState(state);
            this.ActionTakenToGetHere = takenAction;
        }

        public Actions.MoveAction ActionTakenToGetHere { get; }
        public Node Parent { get; set; }
        public IList<Node> Children { get; } = new List<Node>(4);
        public float Cost { get;}
        public PuzzleState State { get; }

        public bool DeadEnd { get; set; } = false;

        public int CompareTo(object obj)
        {
            return Cost.CompareTo(obj);
        }
    }
}
