﻿using CliFx;
using System.Threading.Tasks;

namespace ShiftPuzzle
{
    public static class Program
    {
        public static async Task<int> Main()
        {
            return await new CliApplicationBuilder().AddCommand(typeof (Commands.DefaultCommand)).Build().RunAsync();
        }
    }
}
