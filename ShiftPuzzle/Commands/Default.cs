﻿using CliFx;
using CliFx.Attributes;
using ShiftPuzzle.Evaluators;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace ShiftPuzzle.Commands
{
    [Command]
    class DefaultCommand : ICommand
    {
        [CommandParameter(0, Name = "X Count", Description = "How many horizontal tiles are there?")]
        public uint XCount { get; set; } = 3;

        [CommandParameter(1, Name = "Y Count", Description ="How many vertical tiles are there?")]
        public uint YCount { get; set; } = 3;

        [CommandOption("time", 't', Description = "Log Time it takes to complete the solution")]
        public bool LogTime { get; set; } = false;

        [CommandOption("verbose", 'v', Description = "Log States, updating every second. Only useful for longer operations.")]
        public bool Verbose { get; set; } = false;

        [CommandOption("input", 'i', Description = "Input an initial state. MAKE SURE THAT IT IS SOLVABLE, as the program makes no checks. Input as numberic indices seperated by a dot. For a 3x3 game the values 0 - 8 have to be used. 0 defines the empty field.")]
        public string Input { get; set; } = null;

        [CommandOption("method", 'm', Description = "Define the Heuristic used. Allowed values are 'distance', 'inplace', 'distance+collision")]
        public string Method { get; set; } = "distance";

        public ValueTask ExecuteAsync(IConsole console)
        {
            ShowParameters(console);
            Thread.Sleep(500);
            var stateManager = GenerateStatemanager();
            Task verboseLog = (this.Verbose) ? new Task(() => PrintVerbose(console, stateManager)) : null;
            if (this.Verbose)
            {
                verboseLog.Start();
            }
            Stopwatch sw = (this.LogTime) ? new Stopwatch() : null;

            var result = GetResult(stateManager, sw);
            var resultState = new ResultState
            {
                Method = this.Method,
                StateManager = stateManager,
                ResultNode = result,
                Stopwatch = sw,
            };
            PrintHelper.PrintResult(console, resultState);
            return default;
        }

        private static void PrintVerbose(IConsole console, StateManager stateManager)
        {
            while (!stateManager.Finished)
            {
                stateManager.PrintCurrentState(console);
                Thread.Sleep(1000);
            }
        }

        private StateManager GenerateStatemanager()
        {
            var evaluator = GetEvaluator();

            var initialState = this.Input is null ? new PuzzleState((int)this.XCount, (int)this.YCount, 20000) : new PuzzleState((int)this.XCount, (int)this.YCount, this.Input);

            var stateManager = new StateManager(initialState, evaluator);
            return stateManager;
        }

        private static Node GetResult(StateManager stateManager, Stopwatch sw)
        {
            sw?.Start();
            var result = stateManager.Solve();
            sw?.Stop();
            return result;
        }

        private IEvaluator GetEvaluator()
        {
            return this.Method.Trim().ToLowerInvariant() switch
            {
                "distance" => new DistanceEvaluator(),
                "inplace" => new IsInPlaceEvaluator(),
                "distance+collision" => new DistanceEdgeCountEvaluator(),
                _ => throw new ArgumentException("Given Evaluation Method cannot be recognized")
            };
        }

        private void ShowParameters(IConsole console)
        {
            console.WithForegroundColor(ConsoleColor.DarkCyan, () => { console.Output.WriteLine("\n---\nStarting default command with following parameters:"); });
            console.WithForegroundColor(ConsoleColor.Cyan, () =>
            {
                console.Output.WriteLine($"Horizontal Tilecount: {this.XCount}\nVertical Tilecount: {this.YCount}\nHeristic: {this.Method}");
            });

            console.WithForegroundColor((this.Verbose) ? ConsoleColor.DarkGreen : ConsoleColor.DarkRed, () => { console.Output.WriteLine("Verbose Output"); });
            console.WithForegroundColor((this.LogTime) ? ConsoleColor.DarkGreen : ConsoleColor.DarkRed, () => { console.Output.WriteLine("Log Time"); });
            console.WithForegroundColor(ConsoleColor.DarkCyan, () => { console.Output.WriteLine("---\n\n"); });
        }



    }
}
