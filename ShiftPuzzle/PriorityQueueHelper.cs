﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiftPuzzle
{
    public static class PriorityQueueHelper
    {
        public static bool[] GetPositionOfIndex(int i)
        {
            LinkedList<bool> actionsFromBack = new LinkedList<bool>();
            
            while(i > 1)
            {
                actionsFromBack.AddFirst(i % 2 == 1); // true -> Right ; false -> Left
                if(i % 2 == 1)
                {
                    i--;
                }
                i /= 2;
            }
            return actionsFromBack.ToArray();
        }
    }
}
