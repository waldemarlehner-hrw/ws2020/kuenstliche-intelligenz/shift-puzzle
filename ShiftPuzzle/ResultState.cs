﻿using System.Diagnostics;

namespace ShiftPuzzle
{
    internal class ResultState
    {
        internal Node ResultNode { get; set; }

        internal StateManager StateManager { get; set; }

        internal Stopwatch Stopwatch { get; set; }

        internal string Method { get; set; }
    }
}
