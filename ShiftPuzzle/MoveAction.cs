﻿namespace ShiftPuzzle
{
    public class Actions
    {
        /// <summary>
        /// Direction in which a block moves. The "gap", as in, the 0-Value Element moves in the opposite direction.
        /// <br/>
        /// -> if the Action "Down" is taken, the element in, for example, (4,5) moves to (4,6), 
        /// while the 0-Value Element moves from (4,6) to (4,5)
        /// </summary>
        public enum MoveAction
        {
            Undefined = -1,
            Up,
            Down,
            Left,
            Right
        }
    }
}
