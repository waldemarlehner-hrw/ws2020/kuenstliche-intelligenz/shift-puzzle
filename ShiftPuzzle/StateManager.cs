﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CliFx;
using Priority_Queue;
using ShiftPuzzle.Evaluators;

namespace ShiftPuzzle
{
    public class StateManager
    {
        private readonly SimplePriorityQueue<Node> priorityQueue;
        private readonly IEvaluator evaluator;
        private readonly Node root;
        private readonly HashSet<string> hashSet;
        private Node currentBest;

        public bool Finished { get; private set; } = false;
        public long CheckedCount => this.hashSet.Count;
        public long PrioQueueCount => this.priorityQueue.Count;

        public StateManager(PuzzleState initialState, IEvaluator evaluator)
        {
            this.evaluator = evaluator;
            this.root = new Node(initialState, Actions.MoveAction.Undefined, this.evaluator);
            this.priorityQueue = new SimplePriorityQueue<Node>();
            this.priorityQueue.Enqueue(root, root.Cost);
            this.currentBest = this.root;
            this.hashSet = new HashSet<string> { 
                initialState.Identity
            };
        }

        public Node Solve()
        {
            while (priorityQueue.TryDequeue(out var item))
            {
                Parallel.ForEach(item.State.MoveActions, (move, _, index) =>
                {
                    CheckChild(move, item);
                });
                if(this.currentBest.Cost == 0)
                {
                    this.Finished = true;
                    return this.currentBest;
                }
            }
            this.Finished = true;
            return null;
        }

        private void CheckChild(Actions.MoveAction move, Node item)
        {
            var newState = item.State.MakeMove(move);
            if (!this.hashSet.Contains(newState.Identity))
            {
                // If the node doesnt exist yet, generate a new node.
                var newNode = new Node(newState, move, evaluator)
                {
                    Parent = item
                };

                AddNode(item, newState, newNode);

                lock (this.currentBest)
                {
                    if (this.currentBest.Cost > newNode.Cost)
                    {
                        this.currentBest = newNode;
                    }
                }

            }
        }

        private void AddNode(Node item, PuzzleState newState, Node newNode)
        {
            lock (item.Children)
            {
                item.Children.Add(newNode);
            };
            lock (this.priorityQueue)
            {
                this.priorityQueue.Enqueue(newNode, newNode.Cost);
            }
            lock (this.hashSet)
            {
                this.hashSet.Add(newState.Identity);
            }
        }

        public void PrintCurrentState(IConsole console)
        {
            var checkedStateCount = this.hashSet.Count;
            var bestCost = this.currentBest?.Cost ?? -1;
            List<Node> stateChain = new List<Node>();
            var current = this.currentBest;
            while(current != null)
            {
                stateChain.Add(current);
                current = current.Parent;
            }
            stateChain.Reverse();
            console.WithForegroundColor(ConsoleColor.DarkYellow, () => console.Output.WriteLine("--------------------------"));
            console.Output.Write("Checked States: ");
            console.WithForegroundColor(ConsoleColor.DarkGreen, () => console.Output.WriteLine(checkedStateCount));
            console.Output.Write("Current Best Cost: ");
            console.WithForegroundColor(ConsoleColor.DarkGreen, () => console.Output.WriteLine(bestCost));
            console.Output.WriteLine("\n\n Current Best Chain:");
            foreach(var node in stateChain)
            {
                console.WithForegroundColor(ConsoleColor.DarkGreen, () => console.Output.Write(node.Cost));
                console.Output.Write(" - ");
                console.WithForegroundColor(ConsoleColor.DarkYellow, () => console.Output.WriteLine(node.State.Identity));
            }
            console.WithForegroundColor(ConsoleColor.DarkYellow, () => console.Output.WriteLine("--------------------------"));


        }
    }
}
