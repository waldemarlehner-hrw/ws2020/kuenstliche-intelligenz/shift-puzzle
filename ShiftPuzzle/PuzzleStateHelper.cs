﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ShiftPuzzle
{
    internal static class PuzzleStateHelper
    {
        internal static (int x, int y) GetCoordinatesFromIndex(int i, int width) => (i % width, i / width);

        internal static int GetIndexFromCoordinates((int x, int y) coords, int width) => coords.x + coords.y * width;
    
        internal static IList<Actions.MoveAction> GetAllowedMovementActions(int index, (int width, int height) dimensions)
        {
            var (x, y) = PuzzleStateHelper.GetCoordinatesFromIndex(index, dimensions.width);
            List<Actions.MoveAction> allowedActions = new List<Actions.MoveAction>(4);
            if (x != 0)
            {
                allowedActions.Add(Actions.MoveAction.Right);
            }
            if (x != dimensions.width - 1)
            {
                allowedActions.Add(Actions.MoveAction.Left);
            }
            if (y != 0)
            {
                allowedActions.Add(Actions.MoveAction.Up);
            }
            if (y != dimensions.height - 1)
            {
                allowedActions.Add(Actions.MoveAction.Down);
            }
            return allowedActions;
        }
    }
}
