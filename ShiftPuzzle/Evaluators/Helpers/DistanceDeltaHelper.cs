﻿namespace ShiftPuzzle.Evaluators.Helpers
{
    internal static class DistanceDeltaHelper
    {
        internal static int GetDelta((int x, int y) pos1, (int x, int y) pos2)
        {
            int dX = (pos1.x < pos2.x) ? pos2.x - pos1.x : pos1.x - pos2.x;
            int dY = (pos1.y < pos2.y) ? pos2.y - pos1.y : pos1.y - pos2.y;
            return dX + dY;
        }
    }
}
