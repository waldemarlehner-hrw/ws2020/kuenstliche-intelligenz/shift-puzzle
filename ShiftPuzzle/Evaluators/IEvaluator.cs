﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShiftPuzzle.Evaluators
{
    public interface IEvaluator
    {
        /// <summary>
        /// Evaluates the Puzzle State. Returns a "cost" to evaluate how good this state is. The lower the cost, the better.
        /// </summary>
        /// <param name="state">The State to evaluate.</param>
        /// <returns>The cost for the given State.</returns>
        float EvaluateState(PuzzleState state);
    }
}
