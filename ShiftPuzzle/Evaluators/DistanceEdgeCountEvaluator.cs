﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ShiftPuzzle.Evaluators
{
    /// <summary>
    /// Similar to the DistanceEvaluator, except that it also checks paths taken. 
    /// </summary>
    public class DistanceEdgeCountEvaluator : IEvaluator
    {
        public float EvaluateState(PuzzleState state)
        {
            int distanceCost = 0;
            var edgeSet = new Dictionary<(int from, int to), int>();
            Parallel.ForEach(state.State, (element, _, index) =>
            {
                var elementDesiredIndex = (element == 0) ? state.State.Length - 1 : element - 1;

                var elemenDesiredPosition = PuzzleStateHelper.GetCoordinatesFromIndex(elementDesiredIndex, state.Dimensions.width);
                var elementCurrentPosition = PuzzleStateHelper.GetCoordinatesFromIndex((int)index, state.Dimensions.width);

                // Get the distanceCost
                var deltaCost = Helpers.DistanceDeltaHelper.GetDelta(elemenDesiredPosition, elementCurrentPosition);
                Interlocked.Add(ref distanceCost, deltaCost);
                CountEdges(edgeSet, GetEdges(elementCurrentPosition, elemenDesiredPosition, state.Dimensions));
            });

            // Check all Added Edges. Remove 1 and multiply the result with itself, add to cost
            Parallel.ForEach(edgeSet.Values, (value) => AddEdgeValues(value, ref distanceCost));
            return distanceCost;
        }

        private static void AddEdgeValues(int value, ref int valueToAddTo)
        {
            if (value > 1)
            {
                int val = value - 1;
                Interlocked.Add(ref valueToAddTo, val * val);
            }
        }

        private static void CountEdges(Dictionary<(int from, int to), int> edgeSet, IEnumerable<(int from, int to)> edges)
        {
            foreach (var edge in edges)
            {
                lock (edgeSet)
                {
                    if (!edgeSet.ContainsKey(edge))
                    {
                        edgeSet[edge] = 1;
                    }
                    else
                    {
                        edgeSet[edge]++;
                    }
                }
            }
        }

        private IEnumerable<(int from, int to)> GetEdges((int x, int y) from, (int x, int y) to, (int width, int height) dimensions)
        {
            List<(int from, int to)> indices = new List<(int from, int to)>();
            // Get horizontal
            for(int x = from.x; x < to.x; x++)
            {
                var fromIndex = PuzzleStateHelper.GetIndexFromCoordinates((x, from.y), dimensions.width);
                var toIndex = PuzzleStateHelper.GetIndexFromCoordinates((x + 1, from.y), dimensions.width);
                indices.Add((fromIndex, toIndex));
            }
            for(int y = from.y; y < to.y; y++)
            {
                var fromIndex = PuzzleStateHelper.GetIndexFromCoordinates((to.x, y), dimensions.width);
                var toIndex = PuzzleStateHelper.GetIndexFromCoordinates((to.x, y + 1), dimensions.width);
                indices.Add((fromIndex, toIndex));
            }
            return indices;
        }
    }
}
