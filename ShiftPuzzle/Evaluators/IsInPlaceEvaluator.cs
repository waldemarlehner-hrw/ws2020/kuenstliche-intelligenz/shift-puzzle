﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ShiftPuzzle.Evaluators
{
    public class IsInPlaceEvaluator : IEvaluator
    {
        public float EvaluateState(PuzzleState state)
        {
            var array = state.State;
            int arrLen = (int)array.Length;
            int cost = 0;
            Parallel.ForEach(array, (element, _, index) => {
                if(!IsInPlace(element, (int)index, arrLen))
                {
                    Interlocked.Increment(ref cost);
                }
            });

            return cost;
        }

        private static bool IsInPlace(int value, int index, int arrLen)
        {

            // Add 1 to Index. 1 should be a [0], 2 at [1], ... , 0 at [arr.len - 1]
            if(value != 0)
                return value == index + 1;
            return index == arrLen - 1;
        }
    }
}
