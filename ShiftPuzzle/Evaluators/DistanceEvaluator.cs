﻿using System.Threading;
using System.Threading.Tasks;

namespace ShiftPuzzle.Evaluators
{
    public class DistanceEvaluator : IEvaluator
    {
        public float EvaluateState(PuzzleState state)
        {
            var array = state.State;
            int arrLen = (int)array.Length;
            int cost = 0;
            Parallel.ForEach(array, (element, _, index) => {
                if(element == 0)
                {
                    element = array.Length;
                }
                var currentPosition = PuzzleStateHelper.GetCoordinatesFromIndex(element-1, state.Dimensions.width);
                var desiredPosition = PuzzleStateHelper.GetCoordinatesFromIndex((int)index, state.Dimensions.width);

                var delta = Helpers.DistanceDeltaHelper.GetDelta(currentPosition, desiredPosition);
                Interlocked.Add(ref cost, (int)delta);
            });
            return cost;
        }
    }
}
