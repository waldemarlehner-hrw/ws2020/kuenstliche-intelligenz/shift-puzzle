﻿using System;
using CliFx;
using System.Collections.Generic;

namespace ShiftPuzzle
{
    internal static class PrintHelper
    {
        internal static void PrintResult(IConsole console, ResultState state)
        {
            var resultColor = state.ResultNode is null ? ConsoleColor.Red : ConsoleColor.Green;
            var chain = GenerateNodeChain(state);
            console.WithForegroundColor(resultColor, () => console.Output.WriteLine("---------------[RESULT]---------------"));
            var resultShortText = $"A solution was {((state.ResultNode is null) ? "not" : string.Empty)} found. {((state.ResultNode is null) ? "" : $"The Solution-Chain is {chain.Count} Nodes long.")}";
            console.WithForegroundColor(ConsoleColor.Gray, () => console.Output.WriteLine(resultShortText));
            console.Output.Write("\n\n\n");
            foreach (var node in chain)
            {
                PrintNode(console, node);
            }
            console.Output.Write("\n\n\n");
            PrintSummary(console, state, chain);

            console.WithForegroundColor(resultColor, () => console.Output.WriteLine("--------------------------------------"));
        }

        private static void PrintSummary(IConsole console, ResultState state, List<Node> chain)
        {
            if (!(state.Stopwatch is null))
            {
                console.Output.Write("TimeSpan: ");
                console.WithForegroundColor(ConsoleColor.Yellow, () => console.Output.WriteLine(state.Stopwatch.Elapsed.TotalSeconds.ToString() + "s"));
            }
            console.Output.Write("Total Touched Nodes: ");
            console.WithForegroundColor(ConsoleColor.Yellow, () => console.Output.WriteLine(state.StateManager.CheckedCount));

            console.Output.Write("Length of Priority Queue at the End: ");
            console.WithForegroundColor(ConsoleColor.Yellow, () => console.Output.WriteLine(state.StateManager.PrioQueueCount));

            console.Output.Write("Heuristic Used: ");
            console.WithForegroundColor(ConsoleColor.Yellow, () => console.Output.WriteLine(state.Method));

            console.Output.Write("Chain Length: ");
            console.WithForegroundColor(ConsoleColor.Yellow, () => console.Output.WriteLine(chain.Count));
        }

        private static List<Node> GenerateNodeChain(ResultState state)
        {
            List<Node> chain = new List<Node>();
            var current = state.ResultNode;
            while (!(current is null))
            {
                chain.Add(current);
                current = current.Parent;
            }


            chain.Reverse();
            return chain;
        }

        private static void PrintNode(IConsole console, Node node)
        {
            if (node.ActionTakenToGetHere != Actions.MoveAction.Undefined)
            {
                var actionName = Enum.GetName(typeof(Actions.MoveAction), node.ActionTakenToGetHere);
                console.WithForegroundColor(ConsoleColor.DarkGray, () => console.Output.WriteLine($"v[ {actionName} ]v"));
                console.Output.Write("Cost: ");
                console.WithForegroundColor(ConsoleColor.Yellow, () => console.Output.WriteLine(node.Cost));
                console.WithForegroundColor(ConsoleColor.DarkYellow, () => console.Output.WriteLine(node.State.Identity));
            }

        }
    }
}
